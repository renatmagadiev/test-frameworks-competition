<?php

class EntityTest extends \PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
    }

    protected function tearDown()
    {
    }

    // tests
    public function testMe()
    {
		$name = 'Renat Magadiev';
		$entity = new \App\Entity();

		$entity->setName($name);

		// Assert
		$this->assertEquals($name, $entity->getName());
    }
}
