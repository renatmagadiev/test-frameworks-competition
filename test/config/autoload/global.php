<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return array(
    'db_global_host' => 'localhost',
    'db_global_salt' => 'SAogZRN5d84NTJrK',
//    'db_global_host' => '192.168.88.246',

    'db' => array(
        'driver' => 'Pdo_Pgsql',
        'hostname' => 'db',
        'username' => 'postgres',
        'dbname' => 'postgres'
    ),


    'acl' => array(
        'roles' => array(
            'admin' => array(),
            'member' => array(),
            'guest' => array()
        )
    ),
    'resource_manager' => array(
        // The URI path to resources directory relative to site's base path
        'resource_uri_path' => 'resource'
    ),

	'assets_bundle' => [
		'production' => false
	]
);
