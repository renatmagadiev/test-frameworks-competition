<?php
return [
	'controllers'  => [
		'invokables' => [
			'Rest\Controller\Rest' => 'Rest\Controller\RestController',
		],
	],
	'router' => array(
		'routes' => array(
			'rest' => array(
				'type'    => 'segment',
				'options' => array(
					'route'    => '/rest[/:id]',
					'constraints' => array(
						'id'     => '[0-9]+',
					),
					'defaults' => array(
						'controller' => 'Rest\Controller\Rest',
					),
				),
			),
		),
	),

	'view_manager' => array(
		'strategies' => array(
			'ViewJsonStrategy',
		),
	),
];