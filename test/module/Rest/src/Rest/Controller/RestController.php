<?php
namespace Rest\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class RestController extends AbstractRestfulController
{
	public function getList()
	{
		return new JsonModel(array('response' => 'result'));
	}


	public function get($id)
	{
		return new JsonModel(array('response' => 'result'));
	}


	public function create($data)
	{
		return new JsonModel(array('response' => 'result'));
	}


	public function update($id, $data)
	{
		return new JsonModel(array('response' => 'result'));
	}


	public function delete($id)
	{
		return new JsonModel(array('response' => 'result'));
	}
}
