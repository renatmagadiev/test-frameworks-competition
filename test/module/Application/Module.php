<?php
namespace Application;

use Zend\Mvc\MvcEvent;

class Module
{

	/**
	 * @var \Zend\ServiceManager\ServiceManager
	 */
	protected $serviceManager;

	/**
	 * @var \Zend\EventManager\EventManager
	 */
	protected $eventManager;

	protected $db;


	public function onBootstrap(MvcEvent $e)
	{

		$this->initAcl($e);
		$e->getApplication()->getEventManager()->attach('route', [$this, 'checkAcl']);
	}


	public function initAcl(MvcEvent $e)
	{

		$acl = new \Zend\Permissions\Acl\Acl();
		$roles = include __DIR__ . '/config/module.acl.roles.php';
		$allResources = [];
		foreach ($roles as $role => $resources) {

			$role = new \Zend\Permissions\Acl\Role\GenericRole($role);
			$acl->addRole($role);

			$allResources = array_merge($resources, $allResources);

			//adding resources
			foreach ($resources as $resource) {
				// Edit 4
				if (!$acl->hasResource($resource))
					$acl->addResource(new \Zend\Permissions\Acl\Resource\GenericResource($resource));
			}
			//adding restrictions
			foreach ($allResources as $resource) {
				$acl->allow($role, $resource);
			}
		}

		//setting to view
		$e->getViewModel()->acl = $acl;

	}


	public function checkAcl(MvcEvent $e)
	{
		$route = $e->getRouteMatch()->getMatchedRouteName();
		//you set your role
		$userRole = 'guest';

		if (!$e->getViewModel()->acl->isAllowed($userRole, $route)) {
			$response = $e->getResponse();
			//location to page or what ever
			$response->getHeaders()->addHeaderLine('Location', $e->getRequest()->getBaseUrl() . '/404');
			$response->setStatusCode(404);

		}
	}


	/**
	 * Returns service manager configuration.
	 *
	 * Note: Here are factories that require instantiated classes because
	 *       these can not be cached.
	 *
	 * @return array
	 */
	public function getServiceConfig()
	{
		return [
			'factories' => [
			],
		];
	}


	public function getConfig()
	{
		return include __DIR__ . '/config/module.config.php';
	}


	public function getAutoloaderConfig()
	{
		return [
			'Zend\Loader\StandardAutoloader' => [
				'namespaces' => [
					__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
				],
			],
		];
	}
}
