<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return [
	'router' => [
		'routes' => [

			'application' => [
				'type'          => 'Segment',
				'options'       => [
					'route'    => '/',
					'defaults' => [
						'__NAMESPACE__' => 'Application\Controller',
						'controller'    => 'Application\Controller\Index',
						'action'        => 'index',
					],
				],
				'may_terminate' => TRUE,
				'child_routes'  => [
				],
			],
		],
	],

	'service_manager' => [
		'abstract_factories' => [
			'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
			'Zend\Log\LoggerAbstractServiceFactory',
		],
		'aliases'            => [
			'translator' => 'MvcTranslator',
		],
	],
	'translator'      => [
		'locale'                    => 'en_US',
		'translation_file_patterns' => [
			[
				'type'     => 'gettext',
				'base_dir' => __DIR__ . '/../language',
				'pattern'  => '%s.mo',
			],
		],
	],
	'controllers'     => [
		'invokables' => [
		],
		'factories'  => [
			'Application\Controller\Index' => 'Application\ControllerFactory\IndexControllerFactory',
		],
	],
	'view_manager'    => [
		'display_not_found_reason' => TRUE,
		'display_exceptions'       => TRUE,
		'doctype'                  => 'HTML5',
		'not_found_template'       => 'error/404',
		'exception_template'       => 'error/index',
		'template_map'             => [
			'admin/layout'            => __DIR__ . '/../../Admin/view/layout/layout.phtml',
			'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
			'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
			'error/404'               => __DIR__ . '/../view/error/404.phtml',
			'error/index'             => __DIR__ . '/../view/error/index.phtml',
		],
		'template_path_stack'      => [
			'application' => __DIR__ . '/../view',
			'admin'       => __DIR__ . '/../../Admin/view',
		],
	],

	'module_layouts' => [
		'Application' => 'layout/layout',
		'Admin'       => 'admin/layout',
	],
	// Placeholder for console routes
	'console'        => [
		'router' => [
			'routes' => [],
		],
	],
];
