<?php

return [
	'guest' => [
		'index',
		'rest'
	],
	'admin' => [
		'admin',
		'application'
	],
];