<?php
require_once(dirname(__FILE__) . '/../vendor/simpletest/simpletest/autorun.php');
require_once(dirname(__FILE__) . '/../vendor/autoload.php');

class Entity extends UnitTestCase
{
	function test()
	{
		$name = 'Renat Magadiev';
		$entity = new \App\Entity();

		$entity->setName($name);

		// Assert
		$this->assertEqual($name, $entity->getName());
	}
}

?>