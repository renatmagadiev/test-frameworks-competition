<?php
use PHPUnit\Framework\TestCase;

class Entity extends TestCase
{

	public function testCanBeNegated()
	{
		$name = 'Renat Magadiev';
		$entity = new \App\Entity();

		$entity->setName($name);

		// Assert
		$this->assertEquals($name, $entity->getName());
	}
}