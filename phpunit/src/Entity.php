<?php

namespace App;

/**
 * Class BaseEntity
 * @package App
 */
class Entity
{

	/**
	 * @var array
	 */
	protected $data = [];


	/**
	 * @param $name
	 *
	 * @return $this
	 */
	public function setName($name)
	{
		$this->data['name'] = $name;
		return $this;
	}


	public function getName()
	{
		return isset($this->data['name']) ? $this->data['name'] : NULL;
	}

}